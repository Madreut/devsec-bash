#! /usr/bin/env bash

# Creator: Madreut (Ohad Maimon)
# Purpose: printing the newly created environmental var called "NAME"
# Ver: 0.0.5
# Environmenal variable is a constant variable known in the OS and can be called anywhere.
# In order to create one on your own, simply use the export command
# [varname]=[value]
######################################


echo "my name is $NAME"
