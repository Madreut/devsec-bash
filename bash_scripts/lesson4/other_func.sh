#! /usr/bin/env bash

######################################
# creator: madreut
# purpose: Learning functions
# ver: 0.0.1
######################################


function check_root_home() {
	ls -ltR /root
}

if [[ $UID -eq 0 ]]
then
	check_root_home
else
	echo "Please run script as root"
fi



