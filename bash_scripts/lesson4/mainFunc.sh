#! /usr/bin/env bash

######################################
# creator: madreut
# purpose: main function 
# ver: 0.0.1
######################################

function main (){
	decor "Ohad the Nehmad"

}


function hello_world(){
	line="################"
	printf "\n$line\n# %s\n$line\n" "Hello World"
}

function decor(){
	line="################"
	printf "\n$line\n# %s\n$line\n" "$@"
}



################# DO NOT REMOVE #####################
main
