#! /usr/bin/env bash

######################################
# creator: madreut
# purpose: learning the funcionality of getopts
# ver: 0.0.1
######################################

while getopts ":hello:bye" options
do 
	case $options in
		hello) echo "-a was invoked";;
		bye) echo "-b was invoked";;
		*) echo "invalid options";;

	esac
done
