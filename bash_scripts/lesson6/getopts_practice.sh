#! /usr/bin/env bash

######################################
# creator: madreut
# purpose: learning the funcionality of getopts
# ver: 0.0.1
######################################

while getopts ":ab" options
do 
	case $options in
		a) echo "-a was invoked";;
		b) echo "-b was invoked";;
		*) echo "invalid options";;

	esac
done
