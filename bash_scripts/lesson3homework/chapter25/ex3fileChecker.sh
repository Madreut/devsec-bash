#! /usr/bin/env bash

##############################
# Creator: Madreut
# Purpose: Recieve an input of the user, check if its a file and exists, escalate write privilege if possible
# Ver: 0.0.1
##############################

# recieve an input from the user and place it in variable
read -p "Enter the desired file (Enter the full path!!!!) " filename

# input validation condition
if [[ -z "$filename" ]]
then
	echo "empty value cannot be searched, plesae enter a file name"
	exit 0
fi

# sleep for style points
sleep 1

# condition to check if file exists
if [[ -f $filename ]]
then
	echo "the file exists! checking ownership and permissions"
	sleep 2
	# checking the current user and the file owner
	src_user=$(whoami)
	file_owner=$(ls -la $filename | awk '{print $3}')
	# comparing if the file ownder and the user are the same
	if [[ $src_user == $file_owner ]]
	then
		# checking for write permissions, grant them if there aren't
		if [[ -w $filename ]]
		then
			echo "you have write permissions on the file"
		else
			echo "you dont have write permissions, we will grant it to you right now"
			sleep 1
			chmod +w $filename
			echo "Write permission is granted, have fun"
		fi
	else
		echo "you are not the owner of the file, good luck next time"
	fi
else
	echo "file not found"
fi


