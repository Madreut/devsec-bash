#! /usr/bin/env bash

#########################
#creator madreut
# purpose:recieve 4 parameters and print them in reverse order
# ver 2.1.2
# fixed wrong if statement (instead of do changed to then like it supposed to be)
# searched online for methods to prints arugments backwards, found the for loop below
#########################

if [[ $# -ne 4 ]]
then
	echo "this script reqires 4 parameters"
	exit 1
fi

for (( i=$#;i>0;i-- ))
do
        echo "bakcwards ${!i}"
done

