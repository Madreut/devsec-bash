#! /usr/bin/env bash

#################################
# creator: madreut
# purpose: recive file names and check if they exist
# ver: 1.0.5
# added a for loop that runs through all the arguments, checks if they exists, and print them
# research regarding this showed something about $@ - i didn't quite understand what it does, it is considered best-practice
#################################

if [[ $# -ne 2 ]]
then
	echo "for this exercise we only recieve 2 files, however the script has the abillity to check the existnese of every argument sent"
	exit 1
fi

for i in $*
do
	if [[ -f $i ]]
	then
		echo "the file $i exists"
	else
		echo "the file $i does not exists"
	fi
done


