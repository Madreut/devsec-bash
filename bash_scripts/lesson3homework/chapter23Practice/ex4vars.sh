#! /usr/bin/env bash
set -x
####################
# creator: madreut
# purpose: Scripting Intro - Homework Variables
# ver 0.0.1
####################

# num1 is a varialbe that contains the number 5
#num2 is a varaible that contains the number 10

num1=5
num2=10

# echo command will print the line written inside the apostrophes
echo "the first variable is $num1, the second one is $num2"

