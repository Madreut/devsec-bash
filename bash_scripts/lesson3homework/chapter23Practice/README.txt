Homework for chapter 23:

Answers:
1. The answer is in the script ex1CityScript.sh
2. Same as 1
3. The answer is in the script ex3KornShell.ksh
4. The answer is in the script ex4vars.sh
5. To perform such action the user should use the command: "source ./ex4vars.sh"
6. a shorter way to do the above is to use the syntax: ". ./ex4vars.sh"
7. The scripts conatins comments as requested.
