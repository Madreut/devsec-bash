#! /usr/bin/env bash
set -x
set -u
####################
#creator is madreut
#purpose: counting amount of .txt files in the current directory
# ver 1.1.1
####################

txt_counter=0
for file_name in *.txt
do
	let txt_counter++
done
echo "total amount of txt files in the directory is $txt_counter"

