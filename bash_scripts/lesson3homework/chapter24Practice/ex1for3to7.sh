#! /usr/bin/env bash

###############################
# creator: madreut
# purpose: learning for loops - Homework exercise 1
# ver 0.0.1
##############################

for i in {3..7}
do
	echo "the number is $i"
done
