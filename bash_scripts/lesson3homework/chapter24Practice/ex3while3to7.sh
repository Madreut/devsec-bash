#! /usr/bin/env bash

###############################
# creator: madreut
# purpose: learning while  loops - Homework exercise 3
# ver 0.0.1
##############################
i=3
while [[ i -lt 8 ]]
do
	echo "the number is $i"
	let i++
done
