#! /usr/bin/env bash

###############################
# creator: madreut
# purpose: learning for loops - Homework exercise 2
# ver 0.0.1
##############################

for i in {1..17000}
do
	echo "the number is $i"
done
