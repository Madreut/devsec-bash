#! /usr/bin/env bash

###############################
# creator: madreut
# purpose: learning until  loops - Homework exercise 4
# ver 0.0.1
##############################
i=8
until [[ i -lt 3 ]]
do
	echo "the number is $i"
	let i--
done
