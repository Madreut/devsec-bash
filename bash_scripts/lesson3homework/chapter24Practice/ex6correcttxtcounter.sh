#! /usr/bin/env bash

####################
#creator is madreut
#purpose: counting amount of .txt files in the current directory and fixing 0 txt files showing 1 bug
# ver 1.1.1
####################
#Disclaimer: i looked at the answers, didn't manage to solve it on my own.
# the below is the explanation of the answer
########################################
#The /dev/null directory is a "black hole" this folder is used to throw unneccassry command results
# and avoid displaying them, it is usually used in the find command to fillter irrelevant results
# (for examples files i dont have permission to r or write)
# the "2" after /dev/null indicates to throw every output (stdout), in our scenario every file that 
# ls command will show will be thrown to the void, using the &1 command we can redirect any stderr to stdout 
# by doing so echo $? will actually have a value.
# basiclly we are doing this line cause we need to use ls command in order to see if there are any .txt files
# but we don't want to see the output of the command (the actual list of files) so we use this sequence of
# commands.
########################################
txt_counter=0
ls *.txt > /dev/null 2>&1
if [ $? -ne 0 ]
then
	echo 'there are 0 .txt files in this directroy'
else
	for file_name in *.txt
	do
		let txt_counter++
	done
	echo "total amount of txt files in the directory is $txt_counter"
fi


