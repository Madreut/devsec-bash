#! /usr/bin/env bash


parsed_file=$(sed '/^$/d' .ENV)
parsed_file=$(sed '/^PASSWORD_SALT/d' .ENV)
parsed_file=$(sed '/^INTERFACE_COMM/d' .ENV)
echo "$parsed_file"
