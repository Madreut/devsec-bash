#! /usr/bin/env bash

######################################
# creator: madreut
# purpose: print all interfaces and their corresponding ip addreses.
# ver: 0.0.1
######################################

iface=$(netstat -i | awk {'print $1'} | tail -n+3)
echo "$iface"

